import torch
import torch.optim as optim
from torch.utils.data import DataLoader,Dataset
import glob
import os
import time

from JRP_Net_batch.model import JRP_Net
from JRP_Net_batch.loss import L_fs,L_ps,L_s,L_sp 
from JRP_Net_batch.utils import read_vtk,get_par_fs_to_36,bilinearResampleSphereSurf

"""hyper-parameters"""
cuda = torch.device('cuda:0')
SE_in_ch = 2
SE_out_ch = 128
PD_out_ch = 36
RD_out_ch = 2
level_chs = [16,32,64]

#root_Parc
root_train = 'D:\计算机视觉\code\JRP_Net\数据'
F_path = "D:\Atlas _03M\L\L_atlas_py.vtk"

batch_size = 3
learning_rate = 5e-4
Loss_weight = {'lambda_cc':1.2,'lambda_s':10.0,'lambda_sp':2.0,'lambda_ps':5.0}
epochs = [200,10,100]

class dataset(Dataset):
    def __init__(self,root):
        self.files = sorted(glob.glob(os.path.join(root, '*.vtk')))
    
    def __getitem__(self,index):
        file_path = self.files[index]
        data = read_vtk(file_path)
        data_tensor = torch.cat((torch.from_numpy(data['sulc'][0:40962]).unsqueeze(1),torch.from_numpy(data['curv'][0:40962]).unsqueeze(1)),1)  # (40962,2)
        # Sulc and curv values were firstly normalized between [−1, 1]
        feat_max = torch.max(data_tensor,0)[0]
        feat_min = torch.min(data_tensor,0)[0]
        for i in range(data_tensor.shape[1]):
            data_tensor[:,i] = 2*(data_tensor[:, i]-feat_min[i])/(feat_max[i]-feat_min[i]) - 1

        par_dic = get_par_fs_to_36()
        for i in range(len(data['par_fs'])):
            data['par_fs'][i] = par_dic[data['par_fs'][i]]
        label_tensor = torch.from_numpy(data['par_fs'][0:40962])

        return data_tensor.float(),label_tensor.long()
    
    def __len__(self):
        return len(self.files)

def get_fixed_surface(F_path):
    F_data =  read_vtk(F_path)
    F = torch.cat((torch.from_numpy(F_data['sulc'][0:40962]).unsqueeze(1),torch.from_numpy(F_data['curv'][0:40962]).unsqueeze(1)),1)  # (40962,2)
    feat_max = torch.max(F,0)[0]
    feat_min = torch.min(F,0)[0]
    for i in range(F.shape[1]):
        F[:,i] = 2*(F[:, i]-feat_min[i])/(feat_max[i]-feat_min[i]) - 1
    F = F.unsqueeze(0)  # (1,40962,2)

    par_dic = get_par_fs_to_36()
    for i in range(len(F_data['par_fs'])):
        F_data['par_fs'][i] = par_dic[F_data['par_fs'][i]]
    gt_PF = torch.from_numpy(F_data['par_fs'][0:40962])  # (40962)
    gt_PF = gt_PF.unsqueeze(0)  # (1,40962)

    return F.float(),gt_PF.long()

Parc_only = dataset(root_Parc)
Parc_only_dataloader = DataLoader(Parc_only,batch_size = 1,shuffle = True)
train_dataset = dataset(root_train)
train_dataloader = DataLoader(train_dataset,batch_size = batch_size,shuffle = True)

F,gt_PF = get_fixed_surface(F_path)

model = JRP_Net(SE_in_ch,SE_out_ch,PD_out_ch,RD_out_ch,level_chs)
model.cuda(cuda)
optimizer_1 = optim.Adam([{'params':model.SE.parameters()},{'params':model.PD.parameters()}],lr=learning_rate)
optimizer_2 = optim.Adam(model.parameters(),lr=learning_rate)
optimizer_3 = optim.Adam(model.parameters(),lr=learning_rate)

def train_step_1(model,M,F,gt_PM,gt_PF):
    """
    M (1,40962,2)  moving surface
    F (1,40962,2)  fixed surface
    gt_PM (1,40962)  manual parcellation maps
    gt_PF (1,40962)
    """
    model.train()
    M,F,gt_PM,gt_PF = M.cuda(cuda),F.cuda(cuda),gt_PM.cuda(cuda),gt_PF.cuda(cuda)
    m1,m2,m3,m4 = model.SE(M)
    PM = model.PD(m4,(m1,m2,m3))
    f1,f2,f3,f4 = model.SE(F)
    PF = model.PD(f4,(f1,f2,f3))

    l_sp = L_sp(PM,gt_PM) + L_sp(PF,gt_PF)
    loss = l_sp

    optimizer_1.zero_grad()
    loss.backward()
    optimizer_1.step()

    return loss.item()

def train_step_23(model,M,F,gt_PM,gt_PF,step,loss_weight):
    """
    M (B,40962,2)  moving surface
    F (B,40962,2)  fixed surface
    gt_PM (B,40962)  manual parcellation maps
    gt_PF (B,40962)
    step (int) 对应文中three steps in Training Strategy
    loss_weight (dict)
    """
    assert step in [2,3]

    model.train()
    M,F,gt_PM,gt_PF = M.cuda(cuda),F.cuda(cuda),gt_PM.cuda(cuda),gt_PF.cuda(cuda)
    PM,PF,u_3d,phi = model(M,F)
    # (B,40962,36) (B,40962,36) (B,40962,2) (B,40962,36) (B,40962,3)
    
    l_sp = L_sp(PM,gt_PM) + L_sp(PF,gt_PF)
    
    F_fixed_inter = bilinearResampleSphereSurf(phi,F)  # (B,40962,2)
    l_fs = 0.75*L_fs(F_fixed_inter[:,:,0],M[:,:,0],loss_weight['lambda_cc']) + 0.25*L_fs(F_fixed_inter[:,:,1],M[:,:,1],loss_weight['lambda_cc'])
    l_s = L_s(u_3d)
    if step == 2:
        # by optimizing L_fs + λs*L_s + λsp*L_sp
        loss = l_fs + loss_weight['lambda_s']*l_s + loss_weight['lambda_sp']*l_sp

        optimizer_2.zero_grad()
        loss.backward()
        optimizer_2.step()

        return loss.item()

    else:
        # optimize the full objective L_fs + λs*L_s + λsp*L_sp + λps*L_ps
        PF_fixed_inter = bilinearResampleSphereSurf(phi,PF)  # (B,40962,36)
        l_ps = L_ps(PF_fixed_inter,PM)
        loss = l_fs + loss_weight['lambda_s']*l_s + loss_weight['lambda_sp']*l_sp + loss_weight['lambda_ps']*l_ps

        optimizer_3.zero_grad()
        loss.backward()
        optimizer_3.step()

        return loss.item()

print('Train begin!!!')
t1 = time.time()
# step 1
print('----------step = 1----------')
for epoch in range(epochs[0]):
    print("----epoch:{}----".format(epoch+1))
    for batch_idx,(M,gt_PM) in enumerate(Parc_only_dataloader):
        t3 = time.time()
        loss = train_step_1(model,M,F,gt_PM,gt_PF)
        t4 = time.time()
        print("{}/{} Loss = {:.4} 用时:{:.4}s".format(batch_idx+1,len(Parc_only_dataloader),loss,t4-t3))

F = F.repeat(batch_size,1,1)  # (B,40962,2)
gt_PF = gt_PF.unsqueeze(0).repeat(batch_size,1)  # (B,40962)

# step 2 or 3
for step in range(1,3):
    print('----------step = {}----------'.format(step+1)) 
    for epoch in range(epochs[step]):
        print("----epoch:{}----".format(epoch+1))
        for batch_idx,(M,gt_PM) in enumerate(train_dataloader):
            t5 = time.time()
            loss = train_step_23(model,M,F,gt_PM,gt_PF,step+1,loss_weight = Loss_weight)
            t6 = time.time()

            print("{}/{} Loss = {:.4} 用时:{:.4}s".format(batch_idx+1,len(train_dataloader),loss,t6-t5))
    
    print('-----step {} has finished----'.format(step+1))

print('Train finish!!!')
t2 = time.time()
print("用时:{:.2f}小时".format((t2-t1)/3600))

torch.save(model.state_dict(), os.path.join(root_train,'trained_models.pkl'))