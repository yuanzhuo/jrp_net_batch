import scipy.io as sio 
import numpy as np
import torch
import pyvista
import math

def get_batch_indices(batch_size,indices):
    """
    Input:
        batch_size (int)
        indices (N)
    Output:
        batch_indices (B,N)
        idx (B,N)
    """
    idx = torch.from_numpy(indices).repeat(batch_size,1)  # (B,N)
    view_shape = list(idx.shape)  
    view_shape[1:] = [1] * (len(view_shape) - 1)  # view_shape = [B,1]
    repeat_shape = list(idx.shape)
    repeat_shape[0] = 1  # repeat_shape = [1,N]
    batch_indices = torch.arange(batch_size, dtype=torch.long).view(view_shape).repeat(repeat_shape)  # (B,N)

    return batch_indices,idx

def get_par_fs_to_36():
    """ Preprocessing for parcellatiion label """
    file = "D:/计算机视觉/code/Spherical U-Net1.0/Spherical_U-Net/neigh_indices/sphere_163842.vtk"
    data = read_vtk(file)
    par_fs = data['par_fs']
    par_fs_label = np.sort(np.unique(par_fs))  
    par_dic = {}
    for i in range(len(par_fs_label)):
        par_dic[par_fs_label[i]] = i
    return par_dic

def get_neighs_order(order_path):
    adj_mat_order = sio.loadmat(order_path)
    adj_mat_order = adj_mat_order['adj_mat_order']
    neigh_orders = np.zeros((len(adj_mat_order), 7))
    neigh_orders[:,0:6] = adj_mat_order-1
    neigh_orders[:,6] = np.arange(len(adj_mat_order))
    neigh_orders = np.ravel(neigh_orders).astype(np.int64)  # np.ravel()将多维数组降维为一维
    
    return neigh_orders

def Get_neighs_order():  
    # neigh_indices/adj_mat_order_n.mat预先存储了由20面体拓展出来的各种多面体的各个顶点的临近点的序号，数据大小为n*6
    # 所得neigh_orders_n包含数据大小为n*7
    
    neigh_orders_40962 = get_neighs_order("D:/计算机视觉/code/Spherical U-Net/infantsurfparc/neigh_indices/adj_mat_order_40962.mat")
    neigh_orders_10242 = get_neighs_order("D:/计算机视觉/code/Spherical U-Net/infantsurfparc/neigh_indices/adj_mat_order_10242.mat")
    neigh_orders_2562 = get_neighs_order("D:/计算机视觉/code/Spherical U-Net/infantsurfparc/neigh_indices/adj_mat_order_2562.mat")
    neigh_orders_642 = get_neighs_order("D:/计算机视觉/code/Spherical U-Net/infantsurfparc/neigh_indices/adj_mat_order_642.mat")
    
    return neigh_orders_40962, neigh_orders_10242, neigh_orders_2562, neigh_orders_642

def get_upconv_index(order_path):  
    # neigh_indices/adj_mat_order_n.mat

    adj_mat_order = sio.loadmat(order_path)
    adj_mat_order = adj_mat_order['adj_mat_order']
    adj_mat_order = adj_mat_order -1
    nodes = len(adj_mat_order)  # n
    next_nodes = int((len(adj_mat_order)+6)/4)  # (n+6)/4
    upconv_top_index = np.zeros(next_nodes).astype(np.int64) - 1  # (n+6)/4
    for i in range(next_nodes):
        upconv_top_index[i] = i * 7 + 6
    upconv_down_index = np.zeros((nodes-next_nodes) * 2).astype(np.int64) - 1  # (n-(n+6)/4)
    for i in range(next_nodes, nodes):
        raw_neigh_order = adj_mat_order[i]
        parent_nodes = raw_neigh_order[raw_neigh_order < next_nodes]  # 从n顶点到n*4-6顶点，这些新生顶点的父节点
        assert(len(parent_nodes) == 2)  # 每个新生顶点有2个父节点(根据定义的反卷积操作，连接2个原来的vertice的edge上会有一个新的vertice)
        for j in range(2):
            parent_neigh = adj_mat_order[parent_nodes[j]]
            index = np.where(parent_neigh == i)[0][0]
            upconv_down_index[(i-next_nodes)*2 + j] = parent_nodes[j] * 7 + index
    
    return upconv_top_index, upconv_down_index

def Get_upconv_index():
    # upconv_top_index_n包含数据大小(n+6)/4，对应父节点
    # upconv_down_index_n包含数据大小(n-(n+6)/4)*2，*2是因为存在overlap的部分，进而进行mean操作
    
    upconv_top_index_40962, upconv_down_index_40962 = get_upconv_index("D:/计算机视觉/code/Spherical U-Net/infantsurfparc/neigh_indices/adj_mat_order_40962.mat")
    upconv_top_index_10242, upconv_down_index_10242 = get_upconv_index("D:/计算机视觉/code/Spherical U-Net/infantsurfparc/neigh_indices/adj_mat_order_10242.mat")
    upconv_top_index_2562, upconv_down_index_2562 = get_upconv_index("D:/计算机视觉/code/Spherical U-Net/infantsurfparc/neigh_indices/adj_mat_order_2562.mat")
    
    
    return upconv_top_index_40962, upconv_down_index_40962, upconv_top_index_10242, upconv_down_index_10242,  upconv_top_index_2562, upconv_down_index_2562

def read_vtk(in_file):
    """读取.vtk文件 
    Parameters:
        in_file (str) 文件路径
    Output: 
        data (dict)  'vertices', 'faces', 'curv', 'sulc', ...
    """

    polydata = pyvista.read(in_file)
 
    n_faces = polydata.n_faces
    vertices = np.array(polydata.points)  # get vertices coordinate
    
    # only for triangles polygons data
    faces = np.array(polydata.GetPolys().GetData())  # get faces connectivity
    assert len(faces)/4 == n_faces, "faces number is not consistent!"
    faces = np.reshape(faces, (n_faces,4))
    
    data = {'vertices': vertices,
            'faces': faces
            }
    
    point_data = polydata.point_data
    for key, value in point_data.items():
        if value.dtype == 'uint32':
            data[key] = np.array(value).astype(np.int64)
        elif  value.dtype == 'uint8':
            data[key] = np.array(value).astype(np.int32)
        else:
            data[key] = np.array(value)

    return data

def get_En():
    """获取En,将切平面空间的速度向量映射到三维空间,得到3*2的正交基
    Output
        En (n_ver,6)
        vertices (n_ver,3)  标准40962vertices球体的各个vertice的3维坐标
    """
   
    template = read_vtk("D:/计算机视觉/code/Spherical U-Net1.0/Spherical_U-Net/neigh_indices/sphere_40962.vtk")
    vertices = template['vertices'].astype(np.float64)  # (n_vertices,3)
    
    x_0 = np.argwhere(vertices[:,0]==0)  # np.argwhere(a) 返回非0的数组元组的索引，其中a是要索引数组的条件。
    y_0 = np.argwhere(vertices[:,1]==0)
    inter_ind = np.intersect1d(x_0, y_0)  # 查找两个数组中相同的值，也可用于求两个数组的交集 将x坐标,y坐标都为0的点找出来，即在z轴上的点(2个)
    
    En_1 = np.cross(np.array([0,0,1]), vertices)  # np.cross(a,b) 返回垂直于a和b的向量 (n_vertices,3)
    En_1[inter_ind] = np.array([1,0,0])
    En_2 = np.cross(vertices, En_1)  # (n_vertices,3)
    
    En_1 = En_1/np.repeat(np.sqrt(np.sum(En_1**2, axis=1))[:,np.newaxis], 3, axis=1)  # normalize to unit orthonormal vector  np.sqrt(np.sum(En_1**2, axis=1))得到了含3个元素的向量的模长
    En_2 = En_2/np.repeat(np.sqrt(np.sum(En_2**2, axis=1))[:,np.newaxis], 3, axis=1)  # normalize to unit orthonormal vector  (n_vertices,3)
    En = np.transpose(np.concatenate((En_1[np.newaxis,:], En_2[np.newaxis,:]), 0), (1,2,0))  # (n_vertices,3,2)

    En = torch.from_numpy(En.astype(np.float32))
    En = En.reshape(-1, 6)  # (n_vertices,6)
    fixed_xyz = torch.from_numpy(vertices)  # (n_vertices,3)
    
    return En,fixed_xyz

def convert2DTo3D(u_2d, En):
    """
    Input:
        u_2d (B,N,2)  
        En (N,6)
    Output:
        u_3d (B,N,3) 
    """
    u_3d = torch.zeros(len(u_2d),len(En),3)  # (B,N,3)
    tmp = En * u_2d.repeat(1,1,3)  # (B,N,6)
    u_3d[:,:,0] = tmp[:,:,0] + tmp[:,:,1]  
    u_3d[:,:,1] = tmp[:,:,2] + tmp[:,:,3]
    u_3d[:,:,2] = tmp[:,:,4] + tmp[:,:,5]
    return u_3d

def get_latlon_img(feat):  
    """
    Input:
        feat (B,40962,F)  
    Output:
        img (B,512,512,F)  
    """
    inter_indices = np.load("D:/计算机视觉/code/SphericalUNetPackage-main/sphericalunet/utils/neigh_indices/img_indices_40962_0.npy")
    inter_weights = np.load("D:/计算机视觉/code/SphericalUNetPackage-main/sphericalunet/utils/neigh_indices/img_weights_40962_0.npy")
    inter_indices = torch.from_numpy(inter_indices.astype(np.int64))  # (262144,3)
    inter_weights = torch.from_numpy(inter_weights.astype(np.float32))  # (262144,3)
    width = int(np.sqrt(len(inter_indices)))  # 512
    img = torch.sum((feat[:,inter_indices.flatten(),:]).reshape(len(feat),inter_indices.shape[0],inter_indices.shape[1],-1) * ((inter_weights.unsqueeze(2)).repeat(1,1,feat.shape[-1])), 2)  # (B,262144,F)
    #                                                  (B,262144,3,F)                                                                                     (262144,3,F)                     
    img = img.reshape(len(feat),width,width,-1)  # (B,512,512,F)
    
    return img

def bilinear_interpolate(im, x, y):
    """
    Input:
        im: (B,512,512,F)
        x: (B,40962) 每个元素大小区间[0,511]
        y: (B,40962) 每个元素大小区间[0,511]
    Output:
        (B,40962,F)
    """
    x = torch.clamp(x, 0.0000001, im.shape[1]-1.00000001)  # (B,40962) 大小区间[0.0000001,510.9999999]
    y = torch.clamp(y, 0.0000001, im.shape[1]-1.00000001)
    
    x0 = torch.floor(x)  # (B,40962) 均为整数，大小区间[0,510]
    x1 = x0 + 1  # (B,40962) 均为整数，大小区间[1,511]
    y0 = torch.floor(y)  
    y1 = y0 + 1

    batch_indices = torch.arange(len(im), dtype=torch.long).view(len(im),1).repeat([1,x.shape[1]])  # (B,40962)

    Ia = im[batch_indices,y0.long(),x0.long(),:]  # (B,40962,F)
    Ib = im[batch_indices,y1.long(),x0.long(),:]
    Ic = im[batch_indices,y0.long(),x1.long(),:]
    Id = im[batch_indices,y1.long(),x1.long(),:]

    wa = (x1-x) * (y1-y)  # (B,40962)
    wb = (x1-x) * (y-y0)
    wc = (x-x0) * (y1-y)
    wd = (x-x0) * (y-y0)

    return wa.unsqueeze(2)*Ia + wb.unsqueeze(2)*Ib + wc.unsqueeze(2)*Ic + wd.unsqueeze(2)*Id

def bilinearResampleSphereSurfImg(vertices_inter_raw, img, radius=1.0):
    """
    Input:
        vertices_inter_raw (B,40962,3)
        img (B,512,512,F)
    Output:
        feat_inter (B,40962,F)
    """
    vertices_inter = torch.clone(vertices_inter_raw)  # (B,40962,3)
    
    assert len(vertices_inter.shape) == 3, "len(vertices_inter.shape) == 3"
    assert vertices_inter.shape[-1] == 3, "vertices_inter.shape[-1] == 3"
    
    vertices_inter = vertices_inter/radius  # (B,40962,3)
    
    width = img.shape[1]  # 512

    vertices_inter[:,:,2] = torch.clamp(vertices_inter[:,:,2].clone(), -0.9999999, 0.9999999)  # (B,40962) 各点z坐标值限制在区间[-0.9999999,0.9999999]
    beta = torch.acos(vertices_inter[:,:,2]/1.0)  # (B,40962) cos(β)=z/r β大小区间[0,pi]
    row = beta/(math.pi/(width-1))  # (B,40962) β/(pi/511) row大小区间[0,511]

    alpha = torch.zeros_like(beta)  # (B,40962)
    # prevent divide by 0
    tmp1 = (vertices_inter[:,:,0] == 0).nonzero(as_tuple=True)  #(tuple) 得到vertices_inter中x坐标值等于0的点的index
    vertices_inter[tmp1[0], tmp1[1], 0] = 1e-15
    
    tmp1 = (vertices_inter[:,:,0] > 0).nonzero(as_tuple=True)  #(tuple) 得到vertices_inter中x坐标值大于0的点的index
    alpha[tmp1[0],tmp1[1]] = torch.atan(vertices_inter[tmp1[0], tmp1[1], 1]/vertices_inter[tmp1[0], tmp1[1], 0])  # tan(α)=y/x α[tmp1]大小区间[-pi/2,pi/2]
    
    tmp2 = (vertices_inter[:,:,0] < 0).nonzero(as_tuple=True)  # 得到vertices_inter中x坐标值小于0的点的index
    alpha[tmp2[0],tmp2[1]] = torch.atan(vertices_inter[tmp2[0], tmp2[1], 1]/vertices_inter[tmp2[0], tmp2[1], 0]) + math.pi  # α[tmp2]大小区间[pi/2,3*pi/2]

    alpha = alpha + math.pi * 2  # α大小区间[3*pi/2,7*pi/2]
    alpha = torch.remainder(alpha, math.pi * 2)  # α大小区间[0,2*pi]
    
    if len(tmp1[0]) + len(tmp2[0]) != vertices_inter.shape[0]*vertices_inter.shape[1]:
        print("len(tmp1[0]) + len(tmp2[0]) != num of vertices_inter, subtraction is: ", len(tmp1[0]) + len(tmp2[0]) - vertices_inter.shape[0]*vertices_inter.shape[1])
    
    col = alpha/(2*math.pi/(width-1))  # (B,40962) α/(2*pi/511) col大小区间[0,511]
    
    feat_inter = bilinear_interpolate(img, col, row)
    
    return feat_inter

def bilinearResampleSphereSurf(vertices_inter, feat, radius = 1):  
    """
    Input:
        vertices_inter (B,N,3)
        feat (B,N,F)
    Output:
        (B,N,F)
    """
    img = get_latlon_img(feat)
    
    return bilinearResampleSphereSurfImg(vertices_inter, img)