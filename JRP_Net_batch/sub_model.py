import torch
import torch.nn as nn
from .layers import Conv_Block,mean_pool,trans_conv
from .utils import Get_neighs_order,Get_upconv_index

class Shared_Encoder(nn.Module):
    """Shared_Encoder (SE)
    Parameters:
        in_channel (int)  输入的通道数
        out_channel (int)  输出的通道数
        channels (list) encoder各个level的通道数
    Input
        x (B,40962,2)
    Output
        x1 (B,40962,16)
        x2 (B,10242,32)
        x3 (B,2562,64)
        x4 (B,642,128)
    """
    def __init__(self,in_channel,out_channel,channels):
        super(Shared_Encoder,self).__init__()

        assert len(channels) == 3

        neigh_orders = Get_neighs_order()

        channels = [in_channel] + channels + [out_channel]

        self.conv1 = Conv_Block(channels[0],channels[1],neigh_orders[0])
        self.conv2 = Conv_Block(channels[1],channels[2],neigh_orders[1])
        self.conv3 = Conv_Block(channels[2],channels[3],neigh_orders[2])
        self.conv4 = Conv_Block(channels[3],channels[4],neigh_orders[3])

        self.pool1 = mean_pool(neigh_orders[0])
        self.pool2 = mean_pool(neigh_orders[1])
        self.pool3 = mean_pool(neigh_orders[2])

    def forward(self,x):  
        x1 = self.conv1(x)  # (B,40962,16)
        x_1 = self.pool1(x1)  # (B,10242,16)
        x2 = self.conv2(x_1)  # (B,10242,32)
        x_2 = self.pool2(x2)  # (B,2562,32)
        x3 = self.conv3(x_2)  # (B,2562,64)
        x_3 = self.pool3(x3)  # (B,642,64)
        x4 = self.conv4(x_3)  # (B,642,128)

        return (x1,x2,x3,x4)

class Decoder(nn.Module):
    """Decoder 作为Parcellation Decoder(PD)和Registration Decoder(RD)的通用模板
    Parameters:
        in_channel (int)  输入的通道数
        out_channel (int)  输出的通道数
        channels (list) decoder各个level的通道数
        mode (str) "PD" or "RD" 
    Input
        x  (642,in_channel)
        to_skip (tuple)  encoder各个level产生的结果,将在decoder中做skip connection
    Output
        (40962,out_channel)
    """
    def __init__(self,in_channel,out_channel,channels,mode):
        super(Decoder,self).__init__()
        assert len(channels) == 3
        assert mode in ["PD","RD"]

        neigh_orders = Get_neighs_order()
        upconv_parent_index_40962, upconv_son_index_40962, upconv_parent_index_10242, upconv_son_index_10242,  upconv_parent_index_2562, upconv_son_index_2562 = Get_upconv_index()
         
        channels = [in_channel] + channels + [out_channel]

        self.up1 = trans_conv(channels[0],channels[1],upconv_parent_index_2562,upconv_son_index_2562)
        self.up2 = trans_conv(channels[1],channels[2],upconv_parent_index_10242,upconv_son_index_10242)
        self.up3 = trans_conv(channels[2],channels[3],upconv_parent_index_40962,upconv_son_index_40962)
        
        if mode == "PD":
            self.conv1 = Conv_Block(2*channels[1],channels[1],neigh_orders[2])
            self.conv2 = Conv_Block(2*channels[2],channels[2],neigh_orders[1])
            self.conv3 = Conv_Block(2*channels[3],channels[4],neigh_orders[0])
        elif mode == "RD":
            self.conv1 = Conv_Block(3*channels[1],channels[1],neigh_orders[2])
            self.conv2 = Conv_Block(3*channels[2],channels[2],neigh_orders[1])
            self.conv3 = Conv_Block(3*channels[3],channels[4],neigh_orders[0])

    def forward(self,x,to_skip):  # x (B,642,128)(P)  (B,642,256)(R)
        t3,t2,t1 = to_skip
        #(B,40962,16) (B,10242,32) (B,2562,64) in Parcellation
        #(B,40962,32) (B,10242,64) (B,2562,128) in Registration

        x1 = self.up1(x)  # (B,2562,64)(P,R)  
        x_1 = self.conv1(torch.cat((x1,t1),-1))  # (B,2562,64)(P,R)
        #              (B,2562,128)(P) (B,2562,192)(R)
        x2 = self.up2(x_1)  # (B,10242,32)(P,R)
        x_2 = self.conv2(torch.cat((x2,t2),-1))  # (B,10242,32)(P,R)
        #              (B,10242,64)(P) (B,10242,96)(R)
        x3 = self.up3(x_2)  # (B,40962,16)(P,R)
        x_3 = self.conv3(torch.cat((x3,t3),-1))  # (B,40962,36)(P)  (B,40962,2)(R)
        #              (B,40962,32)(P) (B,40962,48)(R)

        return x_3