import torch
import torch.nn as nn
from .utils import Get_neighs_order,get_batch_indices

def L_fs(F_fixed_inter,M,lambda_cc):
    """Feature Similarity Loss
    Parameters:
        fixed_inter (B,40962)  F(exp(u)(vn))
        moving (B,40962)  M(vn)
        lambda_cc (int)  weight for the correlation coefficient term
    """
    loss_l2 = torch.mean((F_fixed_inter - M)**2,1)  # (B)
    loss_corr = ((F_fixed_inter - F_fixed_inter.mean(1,keepdim=True)) * (M - M.mean(1,keepdim=True))).mean(1) / F_fixed_inter.std(1) / M.std(1)  # (B)
    loss = loss_l2 - lambda_cc * loss_corr  # (B)

    return loss.sum()

def L_s(u_3d):
    """Spherical Deformation Smoothness Loss
    Parameters:
        u_3d (B,40962,3)  
    """
    neigh_orders,_,_,_ = Get_neighs_order()
    batch_indices,idx = get_batch_indices(len(u_3d),neigh_orders)
    grad_filter = torch.ones((7,1))
    grad_filter[6] = -6
    
    loss_smooth = torch.abs(torch.matmul(u_3d[:,:,0][batch_indices,idx].view(len(u_3d),-1,7),grad_filter)) + \
                  torch.abs(torch.matmul(u_3d[:,:,1][batch_indices,idx].view(len(u_3d),-1,7),grad_filter)) + \
                  torch.abs(torch.matmul(u_3d[:,:,2][batch_indices,idx].view(len(u_3d),-1,7),grad_filter))  # (B,40962,1)
    
    return torch.sum(torch.mean(loss_smooth.squeeze(),-1))

def L_sp(pred,gt):
    """Supervised Parcellation Loss
    Parameters:
        pred_P (B,40962,36)  predicted parcellation maps
        gt_P (B,40962)  manual parcellation maps
    """
    loss = nn.CrossEntropyLoss()
    return loss(pred.permute(0,2,1),gt)

def L_ps(PF_fixed_inter,PM):
    """Parcellation Map Similarity Loss
    PF_fixed_inter (B,40962,36)
    PM (B,40962,36)
    """
    softmax = nn.Softmax(dim = -1)
    PF_fixed_inter = softmax(PF_fixed_inter)  # (B,40962,36)
    PM = softmax(PM)  # (B,40962,36)
    dividend = 2*torch.sum(PF_fixed_inter*PM,dim=1)  # (B,36)
    divisor = torch.sum(PF_fixed_inter,dim=1) + torch.sum(PM,dim=1)  # (B,36)
    dice = torch.mean(dividend/divisor,1)  # (B)
    loss = 1 - dice   #  (B) 

    return torch.sum(loss)