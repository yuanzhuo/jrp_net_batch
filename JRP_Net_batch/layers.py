import torch
import torch.nn as nn
from .utils import get_batch_indices,get_En,convert2DTo3D,bilinearResampleSphereSurf

class one_ring_conv(nn.Module):
    """
    Input:
        B x N x D (tensor)
    Output:
        B x N x F (tensor)
    """
    def __init__(self,in_feats,out_feats,neigh_orders):
        super(one_ring_conv,self).__init__()

        self.in_feats = in_feats
        self.neigh_orders = neigh_orders

        self.weight = nn.Linear(7*in_feats,out_feats)  # (7*D,F)
    
    def forward(self,x):  # (B,N,D)
        batch_indices,idx = get_batch_indices(len(x),self.neigh_orders)  # (B,N*7)
        mat = x[batch_indices,idx,:].view(len(x),-1,7*self.in_feats)  # (B,N,7*D)
        out = self.weight(mat)  # (B,N,F)

        return out

class mean_pool(nn.Module):
    """
    Input:
        B x N x D (tensor)
    Output:
        B x (N+6)/4 x D (tensor)
    """
    def __init__(self,neigh_orders):
        super(mean_pool,self).__init__()

        self.neigh_orders = neigh_orders
    
    def forward(self,x):  # (B,N,D)
        num_nodes = int((x.shape[1]+6)/4)  # (N+6)/4
        num_feats = x.shape[-1]  # D

        batch_indices,idx = get_batch_indices(len(x),self.neigh_orders[0:num_nodes*7])  # (B,7*(N+6)/4)
        data = x[batch_indices,idx,:].view(-1,num_nodes,num_feats,7)  # (B,(N+6)/4,D,7)  
        out = torch.mean(data,-1)  # (B,(N+6)/4,D)

        return out  

class trans_conv(nn.Module):
    """
    Input:
        B x N x D (tensor)
    Output:
        B x (N*4-6) x F (tensor)
    """
    def __init__(self,in_feats,out_feats,upconv_parent_indexs,upconv_son_indexs):
        super(trans_conv,self).__init__()

        self.out_feats = out_feats
        self.upconv_parent_indexs = upconv_parent_indexs  # N
        self.upconv_son_indexs = upconv_son_indexs  # ((N*4-6)-N)*2

        self.weight = nn.Linear(in_feats,out_feats*7)  # (D,7*F)

    def forward(self,x):  # (B,N,D)
        x = self.weight(x)  # (B,N,7*F)
        x = x.view(len(x),-1,self.out_feats)  # (B,N*7,F)

        parent_batch_indices,parent_idx = get_batch_indices(len(x),self.upconv_parent_indexs)  # (B,N)
        son_batch_indices,son_idx = get_batch_indices(len(x),self.upconv_son_indexs)  # (B,((N*4-6)-N)*2)

        x_parent = x[parent_batch_indices,parent_idx,:]  # (B,N,F)
        x_son = x[son_batch_indices,son_idx,:].view(len(x),-1,self.out_feats,2)  # (B,(N*4-6)-N,F,2)
        x_son = torch.mean(x_son,-1)  # (B,(N*4-6)-N,F)

        out = torch.cat((x_parent,x_son),1)  # (B,N*4-6,F)
        
        return out
    
class Conv_Block(nn.Module):
    """
    1-ring-Conv + BN + ReLU
    """
    def __init__(self,in_channel,out_channel,neigh_orders):
        super(Conv_Block,self).__init__()

        self.conv = one_ring_conv(in_channel,out_channel,neigh_orders)
        self.bn = nn.BatchNorm1d(out_channel)
        self.af = nn.ReLU()
    
    def forward(self,x):  # (B,N,D)
        x = self.conv(x)  # (B,N,F)
        x = self.bn(x.permute(0,2,1))  # (B,F,N)
        x = self.af(x.permute(0,2,1))  # (B,N,F)

        return x

class scaling_and_squaring_layer(nn.Module):
    """
    Parameters:
        T (int)
    Input:
        u_2d (B,40962,2)
    Output:
        u_3d (B,40962,3)
        phi (B,40962,3)
    """
    def __init__(self,T):
        super(scaling_and_squaring_layer,self).__init__()

        self.T = T
        self.En,self.fixed_xyz = get_En()  # self.En (40962,6) self.fixed_xyz (40962,3)

    def forward(self,u_2d):  # (B,40962,2)
        u_3d = convert2DTo3D(u_2d,self.En)  # (B,40962,3)

        # 初始变形场Φ(1/2^T)
        phi = self.fixed_xyz + u_3d/(2**self.T)  # (B,40962,3)
        phi = phi / (torch.norm(phi,dim=-1,keepdim=True).repeat(1,1,3))  # (B,40962,3) Φ(1/2^T)(vn) = (vn + En_un/2^T) / ||vn + En_un/2^T||

        # compute exp 即Φ(1/2^(t-1)) = Φ(1/2^t)。Φ(1/2^t)
        for _ in range(self.T):
            phi = bilinearResampleSphereSurf(phi,phi.clone())  # (B,40962,3)
            phi = phi / (torch.norm(phi,dim=-1,keepdim=True).repeat(1,1,3))  # (B,4062,3)
        
        return u_3d,phi