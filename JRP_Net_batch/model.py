import torch
import torch.nn as nn
from .sub_model import Shared_Encoder,Decoder
from .layers import scaling_and_squaring_layer

class JRP_Net(nn.Module):
    """ Joint Registration and Parcellation Network
    Parameters:
        SE_in_ch (int) SE的输入通道数
        SE_out_ch (int) SE的输出通道数
        PD_out_ch (int)  PD的输出通道数
        RD_out_ch (int)  RD的输出通道数
        level_chs (list)  encoder,decoder的各个level的通道数
    Input
        F  (B,40962,2)  fixed surface
        M  (B,40962,2)  moving surface
    Output
        PF  (B,40962,36) 
        PM  (B,40962,36)
        F_fixed_inter (B,40962,2)  F(Φ(vn))
        PF_fixed_inter (B,40962,36)  PF(Φ(vn))
        u_3d (B,40962,3)  用于Ls的计算
    """
    def __init__(self,SE_in_ch,SE_out_ch,PD_out_ch,RD_out_ch,level_chs):
        super(JRP_Net,self).__init__()

        assert len(level_chs) == 3

        PD_in_ch = SE_out_ch
        RD_in_ch = SE_out_ch*2

        self.SE = Shared_Encoder(SE_in_ch,SE_out_ch,level_chs)
        self.PD = Decoder(PD_in_ch,PD_out_ch,level_chs[::-1],'PD')
        self.RD = Decoder(RD_in_ch,RD_out_ch,level_chs[::-1],'RD')
        self.scaling_and_squaring = scaling_and_squaring_layer(6)

    def forward(self,M,F):
        m = self.SE(M)
        m1,m2,m3,m4 = m
        # (B,40962,16) (B,10242,32) (B,2562,64) (B,642,128)
        PM = self.PD(m4,(m1,m2,m3))  # (B,40962,36)

        f = self.SE(F)
        f1,f2,f3,f4 = f
        # (B,40962,16) (B,10242,32) (B,2562,64) (B,642,128)
        PF = self.PD(f4,(f1,f2,f3))  # (B,40962,36)
        
        # velocity field
        u_2d = self.RD(torch.cat((m4,f4),-1),(torch.cat((m1,f1),-1),torch.cat((m2,f2),-1),torch.cat((m3,f3),-1)))  # (B,40962,2)
        #                  (B,642,256)             (B,40962,32)          (B,10242,64)            (B,2562,128)

        u_3d,phi = self.scaling_and_squaring(u_2d)  # (B,40962,3)

        return PM,PF,u_3d,phi