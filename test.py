import torch
import torch.nn as nn
from torch.utils.data import DataLoader,Dataset
import numpy as np
import glob
import os
import math

from JRP_Net_batch.model import JRP_Net
from JRP_Net_batch.utils import read_vtk,get_par_fs_to_36,bilinearResampleSphereSurf

root_test = 'D:\计算机视觉\code\JRP_Net\数据'

SE_in_ch = 2
SE_out_ch = 128
PD_out_ch = 36
RD_out_ch = 2
level_chs = [16,32,64]

F_path = "D:\Atlas _03M\L\L_atlas_py.vtk"

model = JRP_Net(SE_in_ch,SE_out_ch,PD_out_ch,RD_out_ch,level_chs)
model_path = "D:/计算机视觉/code/JRP_Net/数据/trained_models.pkl"
model.load_state_dict(torch.load(model_path))
model.eval()

class dataset(Dataset):
    def __init__(self,root):
        self.files = sorted(glob.glob(os.path.join(root, '*.vtk')))
    
    def __getitem__(self,index):
        file_path = self.files[index]
        data = read_vtk(file_path)
        data_tensor = torch.cat((torch.from_numpy(data['sulc'][0:40962]).unsqueeze(1),torch.from_numpy(data['curv'][0:40962]).unsqueeze(1)),1)
        feat_max = torch.max(F,0)[0]
        feat_min = torch.min(F,0)[0]
        for i in range(F.shape[1]):
            F[:,i] = 2*(F[:, i]-feat_min[i])/(feat_max[i]-feat_min[i]) - 1

        par_dic = get_par_fs_to_36()
        for i in range(len(data['par_fs'])):
            data['par_fs'][i] = par_dic[data['par_fs'][i]]
        label_tensor = torch.from_numpy(data['par_fs'][0:40962])

        return data_tensor.float(),label_tensor.long()
    
    def __len__(self):
        return len(self.files)

def get_fixed_surface(F_path):
    F_data =  read_vtk(F_path)
    F = torch.cat((torch.from_numpy(F_data['sulc'][0:40962]).unsqueeze(1),torch.from_numpy(F_data['curv'][0:40962]).unsqueeze(1)),1)  # (40962,2)
    feat_max = torch.max(F,0)[0]
    feat_min = torch.min(F,0)[0]
    for i in range(F.shape[1]):
        F[:,i] = 2*(F[:, i]-feat_min[i])/(feat_max[i]-feat_min[i]) - 1
    F = F.unsqueeze(0)  # (1,40962,2)

    par_dic = get_par_fs_to_36()
    for i in range(len(F_data['par_fs'])):
        F_data['par_fs'][i] = par_dic[F_data['par_fs'][i]]
    gt_PF = torch.from_numpy(F_data['par_fs'][0:40962])  # (40962)

    F_vertices = torch.from_numpy(F_data['vertices'][0:40962])  # (40962,3)

    return F.float(),gt_PF.long(),F_vertices.float()

def square_distance(source, target, len_limit):
    """
    Input:
        source (40962,3)
        target (40962,3)
        len_limit (int)
    Output:
        indices (40962)
    """
    Ns = source.shape[0]
    Nt = target.shape[0]

    s = 0
    e = len_limit
    iters = math.ceil(Nt/len_limit)
    indices = torch.zeros(Nt,1)
    for i in range(iters):
        Len = len_limit
        if i == iters-1:
            Len = Nt - len_limit*i
            e = Nt
        dist = -2 * torch.matmul(target[s:e,:], source.permute(1,0))  # (len,Ns) = (len,3) * (3,Ns) 当输入有多维时，把多出的一维作为batch提出来，其他部分做矩阵乘法
        dist += torch.sum(target[s:e,:] ** 2, -1).view(Len, 1)
        dist += torch.sum(source ** 2, -1).view(1, Ns)
    
        indices[s:e] = torch.argmin(dist,-1,keepdim=True)  # (len,1)
        s = s + len_limit
        e = e + len_limit

    return indices.squeeze().long()

test_dataset = dataset(root_test)
test_dataloader = DataLoader(test_dataset,batch_size = 1,shuffle = True)
softmax = nn.Softmax(dim = -1)
F,gt_PF,F_vertices = get_fixed_surface(F_path)

parc_dice = torch.zeros(len(test_dataloader))
reg_dice = torch.zeros(len(test_dataloader))
CC_sulc = torch.zeros(len(test_dataloader))
CC_curv = torch.zeros(len(test_dataloader))
for batch_idx, (M,gt_PM) in enumerate(test_dataloader):

    gt_PM = gt_PM.squeeze()  # (40962)
    with torch.no_grad():
        PM,PF,u_3d,phi = model(M,F)
        F_fixed_inter = bilinearResampleSphereSurf(phi,F)  # (1,40962,2)
    M = M.squeeze().float()  # (40962,2)
    F_fixed_inter = F_fixed_inter.squeeze().float()  # (40962,2)
    phi = phi.squeeze().float()  # (40962,3)
    
    # Get parc_dice
    PM = softmax(PM.squeeze())  # (40962,36)
    pred = torch.argmax(PM,-1)  # (40962)
    parc_dice[batch_idx] = 2*torch.sum(torch.eq(pred,gt_PM))/(len(pred)+len(gt_PM))
    # Get reg_dice
    indices = square_distance(phi,F_vertices,15000)  # (40962) 为uniform40962上的每个点找最近的phi上的点
    reg_dice[batch_idx] = 2*torch.sum(torch.eq(gt_PM[indices],gt_PF))/(len(gt_PM)+len(gt_PF))
    # Get CC_sulc and CC_curv
    CC_sulc[batch_idx] = ((F_fixed_inter[:,0] - F_fixed_inter[:,0].mean()) * (M[:,0] - M[:,0].mean())).mean() / F_fixed_inter[:,0].std() / M[:,0].std()  
    CC_curv[batch_idx] = ((F_fixed_inter[:,1] - F_fixed_inter[:,1].mean()) * (M[:,1] - M[:,1].mean())).mean() / F_fixed_inter[:,1].std() / M[:,1].std() 

print("parcellation dice is {:.2f}({:.4f})".format(100*parc_dice.mean().item(),parc_dice.std().item()))
print("registration dice is {:.2f}({:.4f})".format(100*reg_dice.mean().item(),reg_dice.std().item()))
print("registration CC_sulc is {:.2f}({:.4f})".format(100*CC_sulc.mean().item(),CC_sulc.std().item()))
print("registration CC_curv is {:.2f}({:.4f})".format(100*CC_curv.mean().item(),CC_curv.std().item()))